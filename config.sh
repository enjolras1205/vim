#!/bin/bash

cat myvim.sh >> ~/.bashrc 
source myvim.sh
ln -s $VIM_HOME/_vimrc ~/.vimrc
git submodule update --init
vim +PluginInstall +qall
