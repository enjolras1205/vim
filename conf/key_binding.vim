let mapleader = "\\"

" Normal Mode, Visual Mode, and Select Mode,
" use <Tab> and <Shift-Tab> to indent
" @see http://c9s.blogspot.com/2007/10/vim-tips.html
" 普通模式，可视模式和选择模式下使用 <Tab> 和 <Shift-Tab> 键来缩进文本。 在可视模式和选择模式下，可以自动恢复选中的文本。
nmap <tab> v>
nmap <s-tab> v<
vmap <tab> >gv
vmap <s-tab> <gv

" Don't use Ex mode, use Q for formatting
map Q gq

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
" 插入模式下在新起一行后的CTRL-U也能被撤销
inoremap <C-U> <C-G>u<C-U>

" for omni cpp complete
nmap <F2> <Esc>:!ctags -R --c++-kinds=+p --fields=+ialS --extra=+q *<CR>
" ctags do not support -R
" nmap <F2> <Esc>:!find . -name "*.[c\|h]" \| xargs ctags -F tags<CR>
nmap <F5> :call CompileCode()<CR>

map <F6> :call Do_make()<CR>
map <c-F6> :silent make clean<CR>

nmap <F7> :call RunCode()<CR>

"cnext cprev
nnoremap <silent> <leader>] :cnext<CR>  
nnoremap <silent> <leader>[ :cprevious<CR>

" jedi vim
let g:jedi#completions_command = "<C-N>"

" tab
nmap <leader>t :set list!<CR>

" noh
nmap <leader>q :noh<CR>
