" 一些通用的配置
" 不使用vi兼容模式
set nocompatible
set backspace=indent,eol,start
" 默认最大化
au GUIEnter * simalt ~x
" 默认英文
let $LANG = 'en'  "set message language
set langmenu=en   "set menu's language of gvim. no spaces beside '='
" 显示行数
set nu
" 打字时隐藏鼠标
set mousehide
" 设命令行为2行高
set ch=2

" 使用utf-8编码
set encoding=utf-8
set fileencoding=utf-8
" 不使用swp文件，注意，错误退出后无法恢复
set noswapfile          

" 设置tab长度为4个空格
" 自动缩进长度
set shiftwidth=4
" tab键真实缩进长度 用tab长度跟空格补齐
set softtabstop=4
" tab长度
set tabstop=4
" tab转换成空格
" set expandtab!
" tab不转成空格
set noexpandtab

" 无需备份
set nobackup

" 保留50条命令记录
set history=50
" 永远显示光标位置
set ruler
" 命令提示
set showcmd
" 增量搜索 输入的同时就搜索了
set incsearch

" a(all mode)允许鼠标
if has('mouse')
  set mouse=a
endif

if &t_Co > 2 || has("gui_running")
" 开启高亮
  syntax on
" 高亮查询pattern
  set hlsearch
endif

" 自动缩进
set autoindent

":autocmd BufNewFile *.js :write
"             ^          ^ ^
"             |          | |
"             |          | The command to run.
"             |          |
"             |          A "pattern" to filter the event.
"             |
"             The "event" to watch for.
if has("autocmd")
  filetype plugin indent on
  augroup vimrcEx
" 删除老的augroup
  au!
" 自动设置文本宽度
  autocmd FileType text setlocal textwidth=78
" 切换行时跳到上次编辑过的位置
  autocmd BufReadPost *
        \ if line("'\"") > 1 && line("'\"") <= line("$") |
        \   exe "normal! g`\"" |
        \ endif
  augroup END
endif " has("autocmd")

" 比较buffer与磁盘文件的区别
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif

" 自动保存session和viminfo
" 缺省的sessionoptions选项包括：blank,buffers,curdir,folds,help,options,tabpages,winsize
" 也就是会话文件会恢复当前编辑环境的空窗口、所有的缓冲区、当前目录、折叠(fold)相关的信息、帮助窗口、所有的选项和映射、所有的标签页(tab)、窗口大小
" set sessionoptions-=curdir
" au VimLeave * mksession! $VIM_HOME/_session_conf.vim
" au VimLeave * wviminfo! $VIM_HOME/_session_viminfo
" source $VIM_HOME/_session_conf.vim
" rviminfo $VIM_HOME/_session_viminfo

set laststatus=2          " 总是显示状态栏
highlight StatusLine cterm=bold ctermfg=yellow ctermbg=blue
" 获取当前路径，将$HOME转化为~
function! CurDir()
        let curdir = substitute(getcwd(), $HOME, "~", "g")
        return curdir
endfunction
set statusline=\|\ %{CurDir()}\\%f%m%r%h\ \|%=\|\ %l,%c\ %p%%\ \|\ ascii=%b,hex=%b%{((&fenc==\"\")?\"\":\"\ \|\ \".&fenc)}\ \|\ %{$USER}\ @\ %{hostname()}\ 

" 显示tab 空格
set listchars=tab:>-,eol:$

" font
" set guifont=Consolas:h13
if has('gui_running')
	"set guioptions-=T  " no toolbar
	"set lines=60 columns=108 linespace=0
	if has('gui_win32')
		set guifont=DejaVu_Sans_Mono:h12:cANSI
	else
		set guifont=DejaVu\ Sans\ Mono\ 12
	endif
endif

" cmd line height
set cmdheight=1

" wildmode
set wildmode=longest,list
