"配置入口

" 使用vundle 不再用pathogen
"call pathogen#runtime_append_all_bundles()
let cfg_loc=$VIM_HOME."/conf"

exec ":source ".cfg_loc."/general.vim"
exec ":source ".cfg_loc."/vundle_cfg.vim"
exec ":source ".cfg_loc."/key_binding.vim"
exec ":source ".cfg_loc."/colour.vim"
"exec ":source ".cfg_loc."/mswin.vim"
"behave mswin
exec ":source ".cfg_loc."/function.vim"
exec ":source ".cfg_loc."/erlang.vim"
exec ":source ".cfg_loc."/skel.vim"
exec ":source ".cfg_loc."/plugin_cfg.vim"

"exec ":source ".cfg_loc."/project_mhxx.vim"
"exec ":source ".cfg_loc."/project_netease.vim"
