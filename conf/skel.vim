" Vim plugin file
" Author:   enjolras 

if exists('g:loaded_skel') || v:version < 700 || &compatible
	finish
else
	let g:loaded_skel = 1
endif

if !exists('g:skel_replace')
	let g:skel_replace = 1
endif

" makesure place the skel dir in the skel.vim dir
if !exists('g:skel_dir_root')
        let g:skel_dir_root = expand('<sfile>:p:h')
endif

function s:LoadSkeleton(skel_path, skel_name)
	if g:skel_replace
		%delete
	else
		let current_line = line('.')
		call append(line('$'), '')
		normal G
	endif
        let skel_dir = g:skel_dir_root . '/' . a:skel_path
	if exists('g:skel_header')
		execute 'read' skel_dir  . '/' . 'header'
		for [name, value] in items(g:skel_header)
			call s:SubstituteField(name, value)
		endfor
            call s:SubstituteField('date', strftime('%Y-%m-%d'))
		call append(line('$'), '')
		normal G
	endif
	execute 'read' skel_dir . '/' . a:skel_name
	call s:SubstituteField('modulename', expand('%:t:r'))
	call s:SubstituteField('filename', toupper(expand('%:t:r')))
	call s:SubstituteField('fullfilename', expand('%'))
	if g:skel_replace
		normal gg
		delete
	else
		call cursor(current_line, 1)
	endif
endfunction

function s:SubstituteField(name, value)
	sil! execute '%substitute/\$' . toupper(a:name) . '/' . a:value . '/'
endfunction

command ErlangApplication silent call s:LoadSkeleton('erlang_skels', 'application')
command ErlangSupervisor  silent call s:LoadSkeleton('erlang_skels', 'supervisor')
command ErlangGenServer   silent call s:LoadSkeleton('erlang_skels', 'gen_server')
command ErlangGenFsm          silent call s:LoadSkeleton('erlang_skels', 'gen_fsm')
command ErlangGenEvent        silent call s:LoadSkeleton('erlang_skels', 'gen_event')
command ErlangCommonTest  silent call s:LoadSkeleton('erlang_skels', 'common_test')
command ErlangModule  silent call s:LoadSkeleton('erlang_skels', 'module')

command CppConsole  silent call s:LoadSkeleton('cpp_skels', 'console')
command CppHeader  silent call s:LoadSkeleton('cpp_skels', 'headfile')
command CppLeetcode  silent call s:LoadSkeleton('cpp_skels', 'leetcode')

command LpcObject  silent call s:LoadSkeleton('lpc_skels', 'object')
command LpcHeader  silent call s:LoadSkeleton('lpc_skels', 'headfile')
