" easy grep
let g:EasyGrepRecursive = 1
let g:EasyGrepSearchCurrentBufferDir = 0
let g:EasyGrepMode = 2

" ctrl-p
let g:ctrlp_working_path_mode = 'ra'

" ctags
set tags=tags;
set autochdir

" skel
" close vimerl template
let g:loaded_erlang_skel = 0
let skel_replace = 1
let skel_header = {"author" : "enjolras", "email" : "enjolras1205@gmail.com"}

" winmanager
let g:winManagerWindowLayout="NERDTree|TagList"
let g:winManagerWidth = 32
nmap <C-W><C-F> :FirstExplorerWindow<cr>
nmap <C-W><C-B> :BottomExplorerWindow<cr>
let g:AutoOpenWinManager = 0
nmap <silent> <leader>wm :WMToggle<cr>

" NERDTree
map <F4> :NERDTreeToggle<CR>
"NERDTree配置
"map <F4> :NERDTreeMirror<CR>
"map <F4> :NERDTreeToggle<CR>
"map <F1> :NERDTreeToggle<CR>
"map <C-F1> :NERDTreeFind<CR>
"let NERDTreeChDirMode=2  "选中root即设置为当前目录
"let NERDTreeQuitOnOpen=1 "打开文件时关闭树
let NERDTreeShowBookmarks=1 "显示书签
"let NERDTreeMinimalUI=1 "不显示帮助面板
"let NERDTreeDirArrows=1 "目录箭头 1 显示箭头  0传统+-|号
let g:NERDTree_title="[NERD Tree]"
function! NERDTree_Start()
        exec 'NERDTree'
endfunction
function! NERDTree_IsValid()
        return 1
endfunction

" tags list
map <F3> :TlistToggle<CR>
let Tlist_Ctags_Cmd = 'ctags'

" 不同时显示多个文件的 tag ，只显示当前文件的
let Tlist_Show_One_File=1
" 如果 taglist 窗口是最后一个窗口，则退出 vim
let Tlist_Exit_OnlyWindow=1
"让当前不被编辑的文件的方法列表自动折叠起来 
let Tlist_File_Fold_Auto_Close=1
" 把taglist窗口放在屏幕的右侧，缺省在左侧 
let Tlist_Use_Right_Window=0
"显示taglist菜单
let Tlist_Show_Menu=1
"启动vim自动打开taglist
let Tlist_Auto_Open=0

" omni complete
let OmniCpp_NamespaceSearch=2
let OmniCpp_ShowAccess=0
set completeopt=menu
"set completeopt=menu,preview
"autocmd FileType python set omnifunc=pythoncomplete#Complete
"autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
"autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
"autocmd FileType css set omnifunc=csscomplete#CompleteCSS
"autocmd FileType xml set omnifunc=xmlcomplete#CompleteTags
"autocmd FileType php set omnifunc=phpcomplete#CompletePHP
"autocmd FileType c set omnifunc=ccomplete#Complete

" lpc.vim
" conflict with taglist
" let lpc_syntax_for_c=1

" cscope
" use both cscope and ctag for 'ctrl-]', ':ta', and 'vim -t'
set cscopetag
" check cscope for definition of a symbol before checking ctags: set to 1
" if you want the reverse search order.
set csto=0
" add any cscope database in current directory
if filereadable("cscope.out")
	cs add cscope.out  
" else add the database pointed to by environment variable 
elseif $CSCOPE_DB != ""
	cs add $CSCOPE_DB
endif
" show msg when any other cscope db added
set cscopeverbose

" echofunc
" output to status line
let g:EchoFuncShowOnStatus=1

" skel
let g:skel_replace=0
