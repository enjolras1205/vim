set nocompatible                  " be iMproved, required
filetype off                      " required

set rtp+=$VIM_HOME/vimfiles/bundle/vundle/
let path='$VIM_HOME/vimfiles/bundle'
call vundle#rc()

" let Vundle manage Vundle, required
Bundle 'gmarik/vundle'

" The following are examples of different formats supported.
" Keep bundle commands between here and filetype plugin indent on.
" scripts on GitHub repos
Bundle 'kien/ctrlp.vim'
Bundle 'vim-scripts/The-NERD-tree'
Bundle 'vim-scripts/EasyGrep'
Bundle 'vim-scripts/winmanager'
Bundle 'vim-scripts/taglist.vim'
"Bundle 'vim-scripts/vimplate'
Bundle 'vim-scripts/lpc.vim'
Bundle 'vim-scripts/OmniCppComplete'
"Bundle 'vim-scripts/AutoComplPop'
Bundle 'vim-scripts/echofunc.vim'
Bundle 'vim-scripts/cscope.vim'
Bundle 'vim-scripts/a.vim'
Bundle 'ervandew/supertab'
Bundle 'jlanzarotta/bufexplorer'
"Bundle 'xolox/vim-lua-ftplugin'
"Bundle 'xolox/vim-misc'
"Bundle 'enjolras1205/vimerl'
"Bundle 'vim-scripts/Vimerl'
"Bundle 'oscarh/vimerl'
"Bundle 'jimenezrick/vimerl'
"Bundle 'mattn/emmet-vim'
"Bundle 'majutsushi/tagbar'
"Bundle 'pangloss/vim-erlang'
Bundle 'hdima/python-syntax'
Bundle 'davidhalter/jedi-vim'
Bundle 'altercation/vim-colors-solarized'
"Bundle 'vim-scripts/comments.vim'
"Bundle 'vim-scripts/grep.vim'

filetype plugin indent on         " required
