" dc 自动生成 edoc 格式注释
map <leader>dc yy<esc>p<esc>k<esc>i%% @spec <esc>j<esc>^<esc>i%%<cr>%% @doc<cr><esc>

" :help vimerl and look up options
let erlang_folding = 0
let erlang_highlight_bif = 1
let erlang_keywordprg = "erl -man"
let erlang_show_errors = 1
let erlang_completion_cache = 1
let g:loaded_erlang_skel = 0
